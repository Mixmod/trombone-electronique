Groupe Trombone électronique : AUREY Jordan, BOURGOIN Hélène, MINKO Maria & VIZIER Adrien.
Projet dans le cadre de l'UE ARTS NUMERIQUES avec M. ROUDAUT.
Le dépôt Git est constitué du rapport en PDF, du fichier de code nommé sketch_may06a et de ce README.
Le fonctionnement de notre trombone est détaillé dans notre rapport. Il a principalement besoin d'une carte Arduino, d'un bouton qui représente le souffle du musicien, un haut-parleur ou encore une LED.

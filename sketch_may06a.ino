#include <Arduino.h>

const byte PIN_BUZZER = 9;
const int LIGHT_MIN = 270;
const int LIGHT_MAX = 975;

void setup() {
  Serial.begin(9600);

  pinMode(PIN_BUZZER, OUTPUT);

  float light_intensity = analogRead(A0);
  
  delay(1000);
}

void loop() {

  float light_intensity = analogRead(A0);
  Serial.println( log(light_intensity / LIGHT_MIN) * (8 / log(LIGHT_MAX/LIGHT_MIN)));
  //Serial.println(light_intensity);
  float sound = make_sound(light_intensity);

  if (sound > 0) {
    tone(PIN_BUZZER, sound);
  }
  else {
    noTone(PIN_BUZZER);
  }


}



float make_sound(float light_intensity){

  light_intensity = log(light_intensity / LIGHT_MIN) * (8 / log(LIGHT_MAX/LIGHT_MIN));
  
  if (light_intensity > 0 && light_intensity < 1) {
    return 261.63;
  }
  else if (light_intensity > 1 && light_intensity <  2 ) {
    return 293.66;
  }
  else if (light_intensity > 2  && light_intensity < 3 ) {
    return 329.63;
  }
  else if (light_intensity > 3  && light_intensity < 4 ) {
    return 349.33;
  }
  else if (light_intensity > 4  && light_intensity < 5 ) {
    return 392.0;
  }
  else if (light_intensity > 5  && light_intensity < 6 ) {
    return 440.0;
  }
  else if (light_intensity > 6  && light_intensity < 7 ) {
    return 493.88;
  }
  else if (light_intensity > 7  && light_intensity < 8 ) {
    return 523.25;
  }

  else {
      Serial.println("pb make_sound : mauvaise plage de frequence");
      return -1;
  }
}
